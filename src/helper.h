/*
 * common helper functions
 */


#ifndef HELPER_H
#define HELPER_H

// Blink function with telnet output

const long interval = 2000;
int ledState = LOW;
unsigned long previousMillis = 0;

void blinkLED()
{
  unsigned long currentMillis = millis();

  // if enough millis have elapsed
  if (currentMillis - previousMillis >= interval)
  {
    previousMillis = currentMillis;

    // toggle the LED
    ledState = !ledState;
    digitalWrite(0, ledState);

    #ifdef DEBUG
    Serial.print(ledState);

    String ledStateMsg = "LED State = ";
    ledStateMsg += ledState;
    TelnetMsg(ledStateMsg);
    #endif
  }
}

#endif                                                                      // HELPER_H