// see: https://www.nikolaus-lueneburg.de/2017/09/wifi-telnet-server-auf-dem-esp8266/

#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include "helper.h"

//const char* ssid = "lx-office-5";
//const char* password = "Aiguiphohg1Em$uuKeichoa3d";
const char* ssid = "hucky.net";
const char* password = "internet4hucky";

uint8_t i;
bool ConnectionEstablished; // Flag for successfully handled connection
  
#undef DEBUG  
#define MAX_TELNET_CLIENTS 2
#define MAX_MESSAGE_LENGTH 1

WiFiServer TelnetServer(23);
WiFiClient TelnetClient[MAX_TELNET_CLIENTS];

#include "telnethelper.h"

void setup()
{
  Serial.begin(19200);
  //Serial.flush();

  #ifdef DEBUG
  Serial.println("Over The Air and Telnet Example");

  Serial.printf("Sketch size: %u\n", ESP.getSketchSize());
  Serial.printf("Free size: %u\n", ESP.getFreeSketchSpace());
  #endif

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  // ... Give ESP 10 seconds to connect to station.
  unsigned long startTime = millis();
  #ifdef DEBUG
  Serial.print("Waiting for wireless connection ");
  #endif

  while (WiFi.status() != WL_CONNECTED && millis() - startTime < 10000)
  {
    delay(200);
    #ifdef DEBUG
    Serial.print(".");
    #endif
  }
  #ifdef DEBUG
  Serial.println();
  #endif

  while (WiFi.status() != WL_CONNECTED)
  {
    #ifdef DEBUG
    Serial.println("Connection Failed! Rebooting...");
    #endif
    delay(3000);
    ESP.restart();
  }

  //Serial.flush();

  #ifdef DEBUG
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  Serial.println("Starting Telnet server");
  #endif
  TelnetServer.begin();
  TelnetServer.setNoDelay(true);

  pinMode(0, OUTPUT);  // initialize onboard LED as output
  /* attention. the internal LED is on my ESP 01 boards wired to
   * gpio1 which is also tx. So whenever you blink the led you 
   * destroy the serial communication.
   * Make a blink on gpio 0
   */

  // OTA

  // Port defaults to 8266
  // ArduinoOTA.setPort(8266);

  // Hostname defaults to esp8266-[ChipID]
  // ArduinoOTA.setHostname("myesp8266");

  // No authentication by default
  
  ArduinoOTA.setPassword((const char *)"1234");
  
  ArduinoOTA.onStart([]() {
    #ifdef DEBUG
    Serial.println("Start");
    #endif
  });
  
  ArduinoOTA.onEnd([]() {
    #ifdef DEBUG
    Serial.println("\nEnd");
    #endif
  });
  
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    #ifdef DEBUG
    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
    #endif
  });
  ArduinoOTA.onError([](ota_error_t error) {
    #ifdef DEBUG
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
    else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
    else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
    else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
    else if (error == OTA_END_ERROR) Serial.println("End Failed");
    #endif
  });
  
  ArduinoOTA.begin();
  
}



void loop() {
  ArduinoOTA.handle();                                                // Wait for OTA connection
  //blinkLED();                                                         // Blink LED
  Telnet();                                                           // Handle telnet connections
}



