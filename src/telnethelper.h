/* 
 * Telnet helper function
 */

#ifndef TELNETHELPER_H
#define TELNETHELPER_H

void TelnetMsg(String text)
{
  for(i = 0; i < MAX_TELNET_CLIENTS; i++)
  {
    if (TelnetClient[i] || TelnetClient[i].connected())
    {
      TelnetClient[i].println(text);
      #ifdef DEBUG
      Serial.println(text);
      #endif
    }
  }
  delay(10);  // to avoid strange characters left in buffer
}

void NotifyOther(int i, char buf)
{
  // Notify the other client what is typed
  if (i==0)
  {
    // the first client has typed, so send the data also to the second
    TelnetClient[1].print(buf);
  }
  else
  {
    TelnetClient[0].print(buf);
  }
}


void Telnet()
{
  // Cleanup disconnected session
  for(i = 0; i < MAX_TELNET_CLIENTS; i++)
  {
    if (TelnetClient[i] && !TelnetClient[i].connected())
    {
      TelnetMsg("Client disconnected ... terminate session ");
      TelnetClient[i].stop();
    }
  }
  
  // Check new client connections
  if (TelnetServer.hasClient())
  {
    ConnectionEstablished = false; // Set to false
    
    for(i = 0; i < MAX_TELNET_CLIENTS; i++)
    {
      // find free socket
      if (!TelnetClient[i])
      {
        TelnetClient[i] = TelnetServer.available(); 
        
        TelnetClient[i].flush();  // clear input buffer, else you get strange characters
        //Serial.flush();
        
        TelnetClient[i].println("Welcome!");
        
        TelnetClient[i].print("Millis since start: ");
        TelnetClient[i].println(millis());
        #ifdef DEBUG 
        Serial.println("middle of welcome");
        #endif
        TelnetClient[i].print("Free Heap RAM: ");
        TelnetClient[i].println(ESP.getFreeHeap());
  
        TelnetClient[i].println("----------------------------------------------------------------");
        
        ConnectionEstablished = true; 
        TelnetMsg("New Telnet client connected to session ");

        break;
      }
    }

    if (ConnectionEstablished == false)
    {
      TelnetServer.available().stop();
      TelnetMsg("An other user cannot connect ... MAX_TELNET_CLIENTS limit is reached!");
    }
  }

  // check for incomming messages from telnet
  for(i = 0; i < MAX_TELNET_CLIENTS; i++)
  {
    if (TelnetClient[i] && TelnetClient[i].connected())
    {
      if(TelnetClient[i].available())
      {
        // get data from the telnet client
        while(TelnetClient[i].available())
        {
          #ifdef DEBUG
          Serial.print("bytes received from net: ");Serial.println(TelnetClient[i].available());
          #endif
          char received = TelnetClient[i].read();
          Serial.write(received);
        }
      }
    }
  }
  
  // Check to see if anything is available in the serial receive buffer
  if ( Serial.available() > 0)
  {
    while (Serial.available() > 0)
    {
      #ifdef DEBUG
      Serial.println("received serial data");
      #endif
    
      // Read the next available byte in the serial receive buffer
      char inByte = Serial.read();

      for(i = 0; i < MAX_TELNET_CLIENTS; i++)
      {
        if (TelnetClient[i] && TelnetClient[i].connected())
        {
          TelnetClient[i].print(inByte);
        }
      }
    }
  }
}

#endif                                                              // TELNETHELPER_H