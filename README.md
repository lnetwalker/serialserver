# SerialServer

**ESP 01** used as a serial console Server. Based on the **ESPUni** pcb ( https://gitlab.com/lnetwalker/espuni.git ). IT pros know those serial console servers where you have eg 8 serial ports connected to the consoles of servers. Then you're able to connect to a server console over the network without leaving your desk.

When working with embedded devices serial consoles are the normal way to connect to your device. To avoid the mess of lot's of cables on your desk, connect your SerialServer with the console of your embedded device,  power the device with the microUSB cable of the SerialServer and you are set.

An other scenario may be when you have devices in remote places or the length of the console cable is too long for cable connections.

## Detail view:

![SerialServer top view](https://gitlab.com/lnetwalker/serialserver/raw/main/images/SerialServer_top.jpg)

![SerialServer bottom view](https://gitlab.com/lnetwalker/serialserver/raw/main/images/SerialServer_bottom.jpg)

## Setup instructions:

configure your SSID and PSK in the sketch, upload to device, done...

